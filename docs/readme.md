# Documentation

## Syntax

### Script Format

```
[FOR {N}] WHEN/WHENEVER {EventPattern}
{ActionPattern}
[THEN {ActionPattern}]
[THEN {ActionPattern}]
[...]

[FOR {N}] WHILE/WHILEVER {StatePattern}
{ControlPattern}
```

<!-- ### Specification Format

```json
{
  "when": ...
}

{
  "where": ...
}

// event pattern
"..."

```

| Property | Type | Description |
| -------- | ---- | ----------- |
| when     |      |             |
| where    |      |             | -->

## Event Pattern

<!-- TODO: consider "## Event Examples" -->

### Events

Event names should be noun.

#### Examples

Exogenous events:

- `ReadyChange`: The event associated with the `Ready` state change.
- `HumanFaceChange`: The event associated with the `HumanFace` state change.
- `HumanSpeakingChange`: The event associated with the `HumanSpeakingChange` state change.
- `HumanSpeech`: The output of a speech recognizer. A string value.

<!-- missing continuous value -->

Robot action events:

- `{actionName}Result`: The result of a robot action.
- `{actionName}StatusChange`: The event associated with the `{actionName}Status` state change.

### Operators

An operator combines events to create new events.

#### is

```
HumanFaceChange is "visible"
```

#### not

```
HumanFaceChange is not "visible"
```

#### or

```
HumanFaceChange is "visible" or HumanSpeakingChange is True
```

<!-- TODO: define the rest of the operators -->

## State Pattern

<!-- TODO: consider "## State Examples" -->

### Examples

State names should also be noun.

Exogenous states:

- `Ready`: The robot is ready. `True` or `False`.
- `HumanFace`: The visibility of a human face. One of `"visible"` or `"invisible"`.
- `HumanSpeaking`: The output of a voice activity detector. `True` or `False`.
- `LastHumanSpeech`: The last emitted value of the `HumanSpeech` event.

<!-- missing continuous value -->

Robot action states:

- `Last{actionName}Result`: The last emitted value of the `{actionName}Result` event.
- `{actionName}Status`: The status of an action. One of `"active"`, `"succeeded"`, `"failed"`, `"canceled"`.

### is

```
HumanFace is "visible"
```

### not

HumanFace is not "visible"

### and

```
Ready is True and HumanFace is "visible"
```

### or

```
HumanFace is "visible" or LastHumanSpeech is "hello"
```

<!-- TODO: ## Event and State Composition define a composition rules -->

## Action Pattern

### Examples

- `Say "hello"`
- `Gesture "happy"`

### after

```
Say "hello" AND after 500ms Gesture "happy"
```

### and

```
Say "hello" AND Gesture "happy"
```

### or

<!-- TODO: define it -->

### immediately, repeat

Convert an action to controller

## Controller Pattern

```
SetEyePosX HumanFacePosX * 10
```

### springto

Convert a controller to an action

## WHEN

```
WHEN Ready is TRUE
Say "hello world"
```

```
WHEN Start is TRUE
START SetEyePosX HumanFacePosX * 10 // do it once
```

```json
[
  {
    "WHEN": "Start",
    "ACTIONS": {
      "Say": "hello world"
    }
  }
]
```

```
WHEN Start is TRUE
SetEyePosX HumanFacePosX * 10 // do it once
```

### WHENEVER

```
WHENEVER Start is TRUE
Say "hello world"
```

```
WHENEVER Start is TRUE
SetEyePosX HumanFacePosX * 10 // do it once
```

### FOR N

```
FOR 3 WHEN Start is TRUE
Say "hello world"
```

```
FOR 3 WHEN Start is TRUE
SetEyePosX HumanFacePosX * 10 // do it once
```

```json
[
  {
    "WHEN": "Start",
    "ACTIONS": {
      "Say": "hello world"
    }
  }
]
```

### WHILE

```
WHILE HumanFace is Visible
SetEyePosX HumanFacePosX * 10 // continuous
AND SetEyePosY HumanFacePosY * 10
```

```
WHILE HumanFace is Visible
ATONCE Say "hello world" // discrete
```

```json
[
  {
    "when": "Started",
    "actions": {
      "Say": "hello world"
    }
  }
]
```

###

#### Robot Action Events

- `{actionName}Result`

## Robot Actions

### Example Actions

#### Durative

- `say`
- `displayMessage`
- `displayImage`
- `displayButtons`

#### Instantaneous

- `setMessage`
- `setImage`
- `setButtons`

### Conditional Action

### Parallel Action

## Then

When {eventPattern}
{actionPattern}
Then {actionPattern}
