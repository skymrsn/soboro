const { parser } = require("./../");

describe("parser", () => {
  test("0", () => {
    const expected = [
      { trigger: "started", actions: [{ name: "say", value: "hello" }] },
    ];
    const actual = parser.parse(`
+ started
- say="hello"`);
    expect(actual).toEqual(expected);
  });
});
